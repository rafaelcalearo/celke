<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Depoimento[]|\Cake\Collection\CollectionInterface $depoimentos
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Depoimento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="depoimentos index large-9 medium-8 columns content">
    <h3><?= __('Depoimentos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nome_dep') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descricao_dep') ?></th>
                <th scope="col"><?= $this->Paginator->sort('video_um') ?></th>
                <th scope="col"><?= $this->Paginator->sort('video_dois') ?></th>
                <th scope="col"><?= $this->Paginator->sort('video_tres') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($depoimentos as $depoimento): ?>
            <tr>
                <td><?= $this->Number->format($depoimento->id) ?></td>
                <td><?= h($depoimento->nome_dep) ?></td>
                <td><?= h($depoimento->descricao_dep) ?></td>
                <td><?= h($depoimento->video_um) ?></td>
                <td><?= h($depoimento->video_dois) ?></td>
                <td><?= h($depoimento->video_tres) ?></td>
                <td><?= h($depoimento->created) ?></td>
                <td><?= h($depoimento->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $depoimento->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $depoimento->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $depoimento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depoimento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
