<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AutorsSob $autorsSob
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Autors Sob'), ['action' => 'edit', $autorsSob->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Autors Sob'), ['action' => 'delete', $autorsSob->id], ['confirm' => __('Are you sure you want to delete # {0}?', $autorsSob->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Autors Sobs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Autors Sob'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Situations'), ['controller' => 'Situations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Situation'), ['controller' => 'Situations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="autorsSobs view large-9 medium-8 columns content">
    <h3><?= h($autorsSob->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($autorsSob->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Situation') ?></th>
            <td><?= $autorsSob->has('situation') ? $this->Html->link($autorsSob->situation->nome_situacao, ['controller' => 'Situations', 'action' => 'view', $autorsSob->situation->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($autorsSob->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($autorsSob->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($autorsSob->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descricao') ?></h4>
        <?= $this->Text->autoParagraph(h($autorsSob->descricao)); ?>
    </div>
</div>
