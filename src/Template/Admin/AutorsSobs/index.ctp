<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AutorsSob[]|\Cake\Collection\CollectionInterface $autorsSobs
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Autors Sob'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Situations'), ['controller' => 'Situations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Situation'), ['controller' => 'Situations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="autorsSobs index large-9 medium-8 columns content">
    <h3><?= __('Autors Sobs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('titulo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('situation_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($autorsSobs as $autorsSob): ?>
            <tr>
                <td><?= $this->Number->format($autorsSob->id) ?></td>
                <td><?= h($autorsSob->titulo) ?></td>
                <td><?= $autorsSob->has('situation') ? $this->Html->link($autorsSob->situation->nome_situacao, ['controller' => 'Situations', 'action' => 'view', $autorsSob->situation->id]) : '' ?></td>
                <td><?= h($autorsSob->created) ?></td>
                <td><?= h($autorsSob->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $autorsSob->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $autorsSob->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $autorsSob->id], ['confirm' => __('Are you sure you want to delete # {0}?', $autorsSob->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
