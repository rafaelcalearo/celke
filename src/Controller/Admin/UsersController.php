<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\Utility\Security;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['cadastrar', 'logout', 'confEmail', 'recuperarSenha', 'atualizarSenha']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = ['limit' => 5];
        $users = $this->paginate($this->Users);
        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set('user', $user);
    }

    public function perfil()
    {
        //$user = $this->Auth->user();
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id);
        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário cadastrado com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->danger(__('Erro: Usuário não foi cadastrado com sucesso'));
        }
        $this->set(compact('user'));
    }

    use MailerAwareTrait;
    public function cadastrar()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->cod_val_email = Security::hash($this->request->getData('password') . $this->request->getData('email'), 'sha256', false);
            if ($this->Users->save($user)) {

                $user->host_name = Router::fullBaseUrl() . $this->request->getAttribute("webroot") . $this->request->getParam('prefix');

                $this->getMailer('User')->send('cadastroUser', [$user]);
                /*
                $msg = 'Caro(a) <b>' . $user->name . '</b><br><br>Obrigado por se cadastrar.<br><br>';
                $email = new Email('envemail');
                $email->setTo($user->email)
                    ->setProfile('envemail')
                    ->setEmailFormat('html')
                    ->setSubject(sprintf('Bem vindo'))
                    ->send($msg);
                */
                $this->Flash->success(__('Cadastrado realizado com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
            $this->Flash->danger(__('Erro: Cadastrado não foi realizado com sucesso'));
        }
        $this->set(compact('user'));
    }

    public function confEmail($cod_val_email = null)
    {
        $userTable = TableRegistry::get('Users');
        $confEmail = $userTable->getConfEmail($cod_val_email);
        if($confEmail){
            $user = $this->Users->newEntity();
            $user->id = $confEmail->id;
            $user->email_val = '1';
            if($userTable->save($user)){
                $this->Flash->success(__('E-mail confirmado com sucesso!'));
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }else{
                $this->Flash->danger(__('Erro: E-mail não foi confirmado!'));
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
        }else{
            $this->Flash->danger(__('Erro: E-mail não foi confirmado!'));
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário editado com sucesso'));

                //return $this->redirect(['action' => 'index']);
                return $this->redirect(['controller' => 'Users', 'action' => 'view', $id]);
            }
            $this->Flash->danger(__('Erro: Usuário não foi editado com sucesso'));
        }
        $this->set(compact('user'));
    }

    public function alterarFotoUsuario($id = null)
    {
        $user = $this->Users->get($id);
        $imagemAntiga = $user->imagem;

        if($this->request->is(['patch', 'post', 'put'])){
            $user = $this->Users->newEntity();
            //ANTES SEM REDIMENSIONAMENTO DE IMAGEM
            //$user->imagem = $this->Users->slugSingleUpload($this->request->getData()['imagem']['name']);
            //COM REDIMENCIONAMENTO
            $user->imagem = $this->Users->slugUploadImgRed($this->request->getData()['imagem']['name']);
            $user->id = $id;

            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($this->Users->save($user)){
                $destino = WWW_ROOT. "files" . DS . "user" . DS . $id . DS;
                $imgUpload = $this->request->getData()['imagem'];
                $imgUpload['name'] = $user->imagem;
                //ANTES SEM REDIMENSIONAMENTO DE IMAGEM
                //if($this->Users->singleUpload($imgUpload, $destino)){
                //COM REDIMENSIONAMENTO
                if($this->Users->uploadImgRed($imgUpload, $destino, 150, 150)){
                    //if(($imagemAntiga !== null) AND ($imagemAntiga !== $user->imagem)){
                    //    unlink($destino.$imagemAntiga);
                    //}
                    $this->Users->deleteFile($destino, $imagemAntiga, $user->imagem);
                    $this->Flash->success(__('Foto editada com sucesso'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'view', $id]);
                }else{
                    $user->imagem = $imagemAntiga;
                    $this->Users->save($user);
                    $this->Flash->danger(__('Erro: Foto não foi editada com sucesso. Erro ao realizar o upload'));
                }
            }else{
                $this->Flash->danger(__('Erro: Foto não foi editada com sucesso.'));
            }
        }  

        $this->set(compact('user'));
    }

    public function recuperarSenha()
    {
        $user = $this->Users->newEntity();
        if($this->request->is('post')){
            $userTable = TableRegistry::get('Users');
            $recupSenha = $userTable->getRecuperarSenha($this->request->getData()['email']);
            if($recupSenha){
                if($recupSenha->recuperar_senha == ""){
                    $user->id = $recupSenha->id;
                    $user->recuperar_senha = Security::hash($this->request->getData()['email'] . $recupSenha->id . date("Y-m-d H:i:s"), 'sha256', false);
                    $userTable->save($user);
                    $recupSenha->recuperar_senha = $user->recuperar_senha;
                }
                $recupSenha->host_name = Router::fullBaseUrl().$this->request->getAttribute('webroot') . $this->request->getParam('prefix');
                $this->getMailer('User')->send('recuperarSenha',[$recupSenha]);
                //var_dump($recupSenha);
                //exit;
                //$this->Flash->success(__('E-mail encontrado, para enviar o e-mail retire o comentário no arquivo UsersController no método recuperarSenha a parte de enviar o email ($this->getMailer...)!'));
                $this->Flash->success(__('E-mail enviado com sucesso, verifique a sua caixa de entrada!'));
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }else{                
                $this->Flash->danger(__('Erro: Nenhum usuário encontrado com esse e-mail!'));
            }
        }
        $this->set(compact('user'));
    }

    public function atualizarSenha($recuperar_senha = null)
    {        
        $userTable = TableRegistry::get('Users');
        $user = $userTable->getAtulizarSenha($recuperar_senha);
        if($user){
            if($this->request->is(['patch', 'post', 'put'])){
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user->recuperar_senha = null;
                if($this->Users->save($user)){
                    $this->Flash->success(__('Senha alterada com sucesso!'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'login']);
                }else{
                    $this->Flash->danger(__('Erro: A senha não foi editada com sucesso!'));
                }
            }
            $this->set(compact('user'));
        }else{
            $this->Flash->danger(__('Erro: Link inválido!'));
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }        
    }

    public function editSenha($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha do usuário editado com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->danger(__('Erro: A senha do usuário não foi editado com sucesso'));
        }
        $this->set(compact('user'));
    }

    public function editPerfil()
    {
        //RECUPERA SOMENTE O ID DO USUARIO LOGADO       
        $user_id = $this->Auth->user('id');
        //PARA DEPOIS RECUPERAR TUDO
        $user = $this->Users->get($user_id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                //SERVE P/ ATUALIZAR TBM O AUTH C/ AS INFORMACOES EDITADAS NO FORMULARIO DE EDICAO DO PERFIL
                //if($this->Auth->user('id') === $user->id){
                //    $data = $user->toArray();
                //    $this->Auth->setUser($data);
                //}
                $this->Flash->success(__('Perfil editado com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
            }
            $this->Flash->danger(__('Erro: Perfil não foi editado com sucesso'));
        }

        $this->set(compact('user'));
    }

    public function editSenhaPerfil()
    {
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Senha editada com sucesso'));

                return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
            }
            $this->Flash->danger(__('Erro: Senha não foi editada com sucesso'));
        }

        $this->set(compact('user'));
    }

    public function alterarFotoPerfil()
    {
        $user_id = $this->Auth->user('id');
        //TROQUEI ISSO
        //$user = $this->Users->get($user_id, ['contain' => []]);
        //POR ISSO, POIS VI QUE NO CURSO EM UM VIDEO (AULA 38) JA ESTAVA ASSIM!!!
        $user = $this->Users->get($user_id);       
        $imagemAntiga = $user->imagem;

        if($this->request->is(['patch', 'post', 'put'])){
            $user = $this->Users->newEntity();
            //ANTES SEM REDIMENSIONAMENTO DE IMAGEM
            //$user->imagem = $this->Users->slugSingleUpload($this->request->getData()['imagem']['name']);
            //COM REDIMENCIONAMENTO
            $user->imagem = $this->Users->slugUploadImgRed($this->request->getData()['imagem']['name']);
            $user->id = $user_id;

            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($this->Users->save($user)){
                $destino = WWW_ROOT. "files" . DS . "user" . DS . $user_id . DS;
                $imgUpload = $this->request->getData()['imagem'];
                $imgUpload['name'] = $user->imagem;
                //ANTES SEM REDIMENSIONAMENTO DE IMAGEM
                //if($this->Users->singleUpload($imgUpload, $destino)){
                //COM REDIMENSIONAMENTO
                if($this->Users->uploadImgRed($imgUpload, $destino, 150, 150)){
                    //if(($imagemAntiga !== null) AND ($imagemAntiga !== $user->imagem)){
                    //    unlink($destino.$imagemAntiga);
                    //}
                    $this->Users->deleteFile($destino, $imagemAntiga, $user->imagem);
                    $this->Flash->success(__('Foto editada com sucesso'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
                }else{
                    $user->imagem = $imagemAntiga;
                    $this->Users->save($user);
                    $this->Flash->danger(__('Erro: Foto não foi editada com sucesso. Erro ao realizar o upload'));
                }
            }else{
                $this->Flash->danger(__('Erro: Foto não foi editada com sucesso.'));
            }
        }    
        /*
        if($this->request->is(['patch', 'post', 'put'])){
            $user = $this->Users->patchEntity($user, $this->request->data);
            $destino = WWW_ROOT. "files" . DS . "user" . DS . $user_id . DS; 
            $user = $this->Users->newEntity();
            $user->imagem = $this->Users->singleUpload($this->request->getData()['imagem'], $destino);
            if($user->imagem){
                $user->id = $user_id;
                if($this->Users->save($user)){
                    if(($imagemAntiga !== null) AND ($imagemAntiga !== $user->imagem)){
                        unlink($destino.$imagemAntiga);
                    }
                    //if($this->Auth->user('id') === $user->id){
                    //    $user = $this->Users->get($user_id);
                    //    $this->Auth->setUser($user);
                    //}
                    $this->Flash->success(__('Foto editada com sucesso'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
                }                
            }else{
                $this->Flash->danger(__('Erro: Foto não foi editada com sucesso'));             
            }
        }*/   

        //$imagemAntiga = $user->imagem;

        /**
        if($this->request->is(['patch', 'post', 'put'])){
            //var_dump($this->request->getData());
            $nomeImg = $this->request->getData()['imagem']['name'];
            $imgTmp = $this->request->getData()['imagem']['tmp_name'];
            $user = $this->Users->newEntity();
            $user->id = $user_id;
            $user->imagem = $nomeImg;

            $destino = "files\user\\".$user_id."\\".$nomeImg;

            if(move_uploaded_file($imgTmp, WWW_ROOT. $destino)){  
                if(($imagemAntiga !== null) AND ($imagemAntiga !== $user->imagem)){
                    unlink(WWW_ROOT."files\user\\" . $user_id."\\".$imagemAntiga);
                }


                if($this->Users->save($user)){
                    if($this->Auth->user('id') === $user->id){
                        $user = $this->Users->get($user_id, [
                            'contain' => []
                        ]);
                        $this->Auth->setUser($user);
                    }                    

                    $this->Flash->success(__('Foto editada com sucesso'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'perfil']);
                }else{
                    $this->Flash->danger(__('Erro: Foto não foi editada com sucesso'));
                }
            }
        }**/

        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $destino = WWW_ROOT. "files" . DS . "user" . DS . $user->id . DS;
        $this->Users->deleteArq($destino);
        
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('Usuário apagado com sucesso'));
        } else {
            $this->Flash->danger(__('Erro: Usuário não foi apagado com sucesso'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }else {
                //COM O return FUNCIONOU E SEM TAMBEM!
                //return $this->Flash->danger(__('Erro: login ou senha incorreto'));
                $this->Flash->danger(__('Erro: login ou senha incorreto'));
            }
        }
    }

    public function logout()
    {
        $this->Flash->success(__('Deslogado com sucesso!'));
        return $this->redirect($this->Auth->logout());
    }   
}
