-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Tempo de geração: 30-Jun-2020 às 01:17
-- Versão do servidor: 8.0.18
-- versão do PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `celke2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigos`
--

DROP TABLE IF EXISTS `artigos`;
CREATE TABLE IF NOT EXISTS `artigos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `conteudo` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `resumo_publico` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `qnt_acesso` int(11) NOT NULL DEFAULT '0',
  `robot_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `situation_id` int(11) NOT NULL,
  `artigos_tp_id` int(11) NOT NULL,
  `artigos_cat_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `artigos`
--

INSERT INTO `artigos` (`id`, `titulo`, `descricao`, `conteudo`, `imagem`, `slug`, `keywords`, `description`, `resumo_publico`, `qnt_acesso`, `robot_id`, `user_id`, `situation_id`, `artigos_tp_id`, `artigos_cat_id`, `created`, `modified`) VALUES
(1, 'Lorem Ipsum 1', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>', '<p>Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32.</p><p>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>', 'empresa1.jpg', 'lorem-ipsum-um', 'artigo um', 'Lorem Ipsum um', '<p><strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>', 2, 3, 1, 1, 1, 1, '2018-12-30 14:58:54', '2020-05-24 06:17:32'),
(2, 'Lorem Ipsum 2', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>', '<p><br>Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32.</p><p>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>', 'adasdasdasda.png', 'lorem-ipsum-dois', 'artigo dois', 'Lorem Ipsum dois', '<p><strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>', 2, 3, 1, 1, 1, 1, '2020-05-24 06:22:10', '2020-05-24 06:22:10'),
(3, 'Lorem Ipsum 3', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>', '<p>Ao contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do \"de Finibus Bonorum et Malorum\" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, \"Lorem Ipsum dolor sit amet...\" vem de uma linha na seção 1.10.32.</p><p>O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.</p>', 'adasdsaas.png', 'lorem-ipsum-tres', 'artigo tres', 'Lorem Ipsum tres', '<p><strong>Lorem Ipsum</strong> é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>', 4, 3, 1, 1, 1, 1, '2020-05-24 06:24:48', '2020-05-24 06:24:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigos_cats`
--

DROP TABLE IF EXISTS `artigos_cats`;
CREATE TABLE IF NOT EXISTS `artigos_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `situation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `artigos_cats`
--

INSERT INTO `artigos_cats` (`id`, `nome`, `situation_id`, `created`, `modified`) VALUES
(1, 'PHP', 1, '2018-12-30 14:54:51', '2018-12-30 14:54:51'),
(2, 'Bootstrap', 1, '2018-12-30 14:55:02', '2018-12-30 14:55:02'),
(3, 'PHP OO', 1, '2018-12-30 14:55:12', '2018-12-30 14:55:12'),
(4, 'CakePHP', 1, '2018-12-30 14:55:20', '2018-12-30 14:55:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigos_tps`
--

DROP TABLE IF EXISTS `artigos_tps`;
CREATE TABLE IF NOT EXISTS `artigos_tps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `artigos_tps`
--

INSERT INTO `artigos_tps` (`id`, `nome`, `created`, `modified`) VALUES
(1, 'Publico', '2018-12-30 14:52:56', '2018-12-30 14:52:56'),
(2, 'Privado', '2018-12-30 14:53:05', '2018-12-30 14:53:05'),
(3, 'Privado com resumo público', '2018-12-30 14:53:27', '2018-12-30 14:53:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `autors_sobs`
--

DROP TABLE IF EXISTS `autors_sobs`;
CREATE TABLE IF NOT EXISTS `autors_sobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `situation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `autors_sobs`
--

INSERT INTO `autors_sobs` (`id`, `titulo`, `descricao`, `situation_id`, `created`, `modified`) VALUES
(1, 'Sobre Autor', '<p>Etiam porta sem <i>malesuada magna</i> mollis euismod. Cras <strong>mattis consectetur</strong> purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>', 1, '2019-01-23 00:00:00', '2019-01-10 10:08:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `carousels`
--

DROP TABLE IF EXISTS `carousels`;
CREATE TABLE IF NOT EXISTS `carousels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_carousel` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_botao` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `color_id` int(11) DEFAULT NULL,
  `situation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `carousels`
--

INSERT INTO `carousels` (`id`, `nome_carousel`, `imagem`, `titulo`, `descricao`, `titulo_botao`, `link`, `ordem`, `position_id`, `color_id`, `situation_id`, `created`, `modified`) VALUES
(1, 'Slide 01', 'imagem-um.jpg', 'Example headline um.', 'Um cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'Visualizar', 'https://celke.com.br', 1, 1, 1, 1, '2020-05-14 00:12:59', '2020-05-15 22:40:10'),
(2, 'Slide 02', 'imagem-dois.jpg', 'Another example headline.', 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'Visualizar', 'https://celke.com.br', 2, 2, 6, 1, '2020-05-14 01:24:37', '2020-05-15 22:30:03'),
(3, 'Slide 03', 'imagem-tres.jpg', 'One more for good measure.', 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.', 'Visualizar', 'https://celke.com.br', 3, 3, 3, 1, '2020-05-14 01:26:24', '2020-05-14 01:28:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_cor` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `colors`
--

INSERT INTO `colors` (`id`, `nome_cor`, `cor`, `created`, `modified`) VALUES
(1, 'Azul', 'primary', '2018-11-29 20:06:34', '2018-11-29 20:06:34'),
(2, 'Cinza', 'secondary', '2018-11-29 20:06:34', NULL),
(3, 'Verde', 'success', '2018-11-29 20:06:34', NULL),
(4, 'Vermelho', 'danger', '2018-11-29 20:06:34', NULL),
(5, 'Laranjado', 'warning', '2018-11-29 20:06:34', NULL),
(6, 'Azul claro', 'info', '2018-11-29 20:06:34', NULL),
(7, 'Claro', 'light', '2018-11-29 20:06:34', NULL),
(8, 'Cinza escuro', 'dark', '2018-11-29 20:06:34', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_msgs`
--

DROP TABLE IF EXISTS `contatos_msgs`;
CREATE TABLE IF NOT EXISTS `contatos_msgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assunto` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `conts_msgs_sit_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_msgs`
--

INSERT INTO `contatos_msgs` (`id`, `nome`, `email`, `assunto`, `mensagem`, `user_id`, `conts_msgs_sit_id`, `created`, `modified`) VALUES
(1, 'Cesar', 'cesar@celke.com.br', 'Mensagem 1', 'Conteúdo da mensagem 1', 1, 3, '2018-12-19 23:35:00', '2020-05-20 06:48:34'),
(3, 'Eminem', 'eminem@hotmail.com', 'Ola', 'Ola', NULL, 2, '2020-05-20 07:18:37', '2020-05-20 07:18:37'),
(4, 'Gojim', 'gojimip882@whowlft.com', 'Ola', 'Ola', NULL, 2, '2020-05-20 07:21:20', '2020-05-20 07:21:20'),
(5, 'Gojim 2', 'gojimip882@whowlft.com', 'Ola', 'Ola', NULL, 2, '2020-05-20 07:22:32', '2020-05-20 07:22:32'),
(6, 'Teste', 'gojimip882@whowlft.com', 'Ola', 'ola', NULL, 2, '2020-05-20 07:23:54', '2020-05-20 07:23:54'),
(7, 'Teste 04', 'gojimip882@whowlft.com', 'Ola', 'OLA', NULL, 1, '2020-05-20 07:37:05', '2020-05-20 07:39:29'),
(8, 'Rafael Calearo', 'rafaelcalearo@yahoo.com.br', 'Ola', 'teste', NULL, 2, '2020-06-30 00:52:52', '2020-06-30 00:52:52'),
(9, 'Rafael Calearo', 'rafaelcalearo@yahoo.com.br', 'Ola', 'teste', NULL, 2, '2020-06-30 00:54:54', '2020-06-30 00:54:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `conts_msgs_sits`
--

DROP TABLE IF EXISTS `conts_msgs_sits`;
CREATE TABLE IF NOT EXISTS `conts_msgs_sits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_sit_msg_cont` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `color_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `conts_msgs_sits`
--

INSERT INTO `conts_msgs_sits` (`id`, `nome_sit_msg_cont`, `color_id`, `created`, `modified`) VALUES
(1, 'Aberto', 1, '2018-12-19 23:32:15', '2018-12-19 23:32:15'),
(2, 'Pendete', 4, '2018-12-19 23:32:35', '2018-12-19 23:32:35'),
(3, 'Respondido', 3, '2018-12-19 23:32:54', '2018-12-19 23:32:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimentos`
--

DROP TABLE IF EXISTS `depoimentos`;
CREATE TABLE IF NOT EXISTS `depoimentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_dep` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao_dep` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `video_um` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `video_dois` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `video_tres` varchar(320) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `depoimentos`
--

INSERT INTO `depoimentos` (`id`, `nome_dep`, `descricao_dep`, `video_um`, `video_dois`, `video_tres`, `created`, `modified`) VALUES
(1, 'Depoimentos', 'This is a wider card with supporting text below as a natural lead-in to additional content.', '<iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/JfAgl6CGg2Q?rel=0\"  frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/4tBeeMcw2sM?rel=0\"  frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '<iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/i_R6sMRRQ0s?rel=0\"  frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>', '2018-12-16 12:27:05', '2020-05-17 21:34:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas_sobs`
--

DROP TABLE IF EXISTS `empresas_sobs`;
CREATE TABLE IF NOT EXISTS `empresas_sobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `situation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresas_sobs`
--

INSERT INTO `empresas_sobs` (`id`, `titulo`, `descricao`, `imagem`, `ordem`, `situation_id`, `created`, `modified`) VALUES
(1, 'Sobre Empresa Um', 'Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.', 'empresa1.jpg', 1, 1, '2018-12-16 00:00:00', '2020-05-18 00:53:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `positions`
--

DROP TABLE IF EXISTS `positions`;
CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_posicao` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `posicao` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `positions`
--

INSERT INTO `positions` (`id`, `nome_posicao`, `posicao`, `created`, `modified`) VALUES
(1, 'Esquerdo', 'text-left', '2018-12-07 00:00:00', NULL),
(2, 'Centralizado', 'text-center', '2018-12-07 00:00:00', NULL),
(3, 'Direito', 'text-right', '2018-12-07 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `redes_socias`
--

DROP TABLE IF EXISTS `redes_socias`;
CREATE TABLE IF NOT EXISTS `redes_socias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icone` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `situation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `redes_socias`
--

INSERT INTO `redes_socias` (`id`, `titulo`, `link`, `icone`, `situation_id`, `created`, `modified`) VALUES
(1, 'Instagram', 'https://www.instagram.com/celkecursos', 'fab fa-instagram', 1, '2019-01-10 10:37:01', '2019-01-10 10:37:01'),
(2, 'Facebook', 'https://www.facebook.com/celkecursos/', 'fab fa-facebook-square', 1, '2019-01-10 11:13:42', '2019-01-10 11:13:42'),
(3, 'YouTube', 'https://www.youtube.com/channel/UC5ClMRHFl8o_MAaO4w7ZYug', 'fab fa-youtube', 1, '2019-01-10 11:14:24', '2019-01-10 11:14:24'),
(4, 'Twiter', 'https://twitter.com/celkecursos', 'fab fa-twitter-square', 1, '2019-01-10 11:15:05', '2019-01-10 11:15:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `robots`
--

DROP TABLE IF EXISTS `robots`;
CREATE TABLE IF NOT EXISTS `robots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `robots`
--

INSERT INTO `robots` (`id`, `nome`, `tipo`, `created`, `modified`) VALUES
(3, 'Indexar a página mas não seguir os links', 'index,nofollow', '2018-12-30 14:47:30', '2018-12-30 14:47:30'),
(2, 'Não indexar a página mas seguir os links', 'noindex,follow', '2018-12-30 14:46:29', '2018-12-30 14:46:29'),
(1, 'Indexar a página e seguir os links', 'index,follow', '2018-12-30 14:45:50', '2018-12-30 14:45:50'),
(4, 'Não indexar a página e nem seguir os links', 'noindex,nofollow', '2018-12-30 14:49:33', '2018-12-30 14:49:33'),
(5, 'Não exibir a versão em cache da página', 'noarchive', '2018-12-30 14:50:13', '2018-12-30 14:50:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

DROP TABLE IF EXISTS `servicos`;
CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_ser` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icone_um` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `titulo_um` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao_um` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icone_dois` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `titulo_dois` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao_dois` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `icone_tres` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `titulo_tres` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descricao_tres` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `titulo_ser`, `icone_um`, `titulo_um`, `descricao_um`, `icone_dois`, `titulo_dois`, `descricao_dois`, `icone_tres`, `titulo_tres`, `descricao_tres`, `created`, `modified`) VALUES
(1, 'Serviços', 'fas fa-plane', 'Serviço um', 'This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.', 'fas fa-train', 'Serviço dois', 'This is a wider card with supporting text below as a natural lead-in to additional content. This card has even content than the second to show that equal height action.', 'fa fa-bus', 'Serviço tres', 'This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content that equal height action.', '2018-12-14 19:14:30', '2020-05-17 01:45:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `situations`
--

DROP TABLE IF EXISTS `situations`;
CREATE TABLE IF NOT EXISTS `situations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_situacao` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `color_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `situations`
--

INSERT INTO `situations` (`id`, `nome_situacao`, `color_id`, `created`, `modified`) VALUES
(1, 'Ativo', 3, '2018-11-23 00:00:00', NULL),
(2, 'Inativo', 4, '2018-11-23 00:00:00', NULL),
(3, 'Analise', 1, '2018-11-23 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cod_val_email` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `recuperar_senha` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_val` int(11) DEFAULT '2',
  `imagem` varchar(220) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `cod_val_email`, `recuperar_senha`, `email_val`, `imagem`, `created`, `modified`) VALUES
(1, 'Rafael Calearo', 'rafaelcalearo@gmail.com', 'rafaelcalearo@gmail.com', '$2y$10$NmK3H9b24VoqCPYD3Pq.je9C234noHgmARBjH3ixbcG6dtsgfZgMq', NULL, NULL, 2, 'rafael.jpg', '2020-05-12 00:03:09', '2020-05-14 00:04:08');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
